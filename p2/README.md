# LIS4381

## Jarod Barclay

### Project 2 Requirements:

*Sub-Heading:*

1. Create a project using server-side validation
2. Add edit/delete functions to A5
3. Chapter Questions (Chs. 13, 14)

#### README.md file should include the following items:

* Screenshot of index.php
* Screenshot of edit_petstore.php
* Screenshot of edit_petstore_process.php
* Screenshot of Carousel
* Screenshot of RSS Feed



#### Assignment Screenshots:

*Screenshots:*

![index.php Screenshot](img/ss1.png)

![edit_petstore Screenshot](img/ss2.png)

![edit_petstore_process Screenshot](img/ss3.png)

![Carousel Screenshot](img/ss4.png)

![RSS Feed Screenshot](img/ss5.png)


