> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - install Android Studio and create My First App
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions
    
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provides screenshots of completed app

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Design database in MySQL using info given
    - Chapter Questions (5,6)
4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create Business Card app
    - Chapter Questions (7,8)
3. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create My Online Portfolio using Carousel
    - Create basic project using Bootstrap client-side validation
    - Chapter Questions (9,10,19)
4. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create basic project using server-side validation
    - Chapter Questions (11,12)
5. [P2 README.md](p2/README.md "My P2 README.md file")
    - Create basic project using server-side validation
    - Add edit/delete functions to A5
    - Carousel
    - Chapter Questions (13,14)