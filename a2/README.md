> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Jarod Barclay

### Assignment 2 Requirements:

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1;
* Screenshot of running applications first user interface;
* Screenshot of running applications second user interface;


#### Assignment Screenshots:

*Screenshot of First User Interface:*

![First User Interface](img/bruschetta.png)

*Screenshot of Second User Interface:*

![Second User Interface](img/recipe.png)

