# LIS4381

## Jarod Barclay

### Assignment 4 Requirements:

*Sub-Heading:*

1. Create an Online Portfolio using Carousel
2. Create a project using Bootstrap client-side validation
2. Chapter Questions (Chs. 9,10,19)

#### README.md file should include the following items:

* Screenshot of My Online Portfolio main page
* Screenshot of failed validation
* Screenshot of successful validation



#### Assignment Screenshots:

*Screenshots:*

![Main Page Screenshot](img/ss1.png)

![Invalid Screenshot](img/ss2.png)

![Valid Screenshot](img/ss3.png)



