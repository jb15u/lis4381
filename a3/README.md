> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Jarod Barclay

### Assignment 3 Requirements:

*Sub-Heading:*

1. Design database in MySQL using information given
2. Chapter Questions (Chs. 5, 6)

#### README.md file should include the following items:

* Screenshot of ERD
* Link to a3.mwb



#### Assignment Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img/a3.png)



#### Assignment 3 Links:

*A3 Workbench Link:*
[A3 Workbench Link](docs/a3.mwb "A3 Workbench Link")

*A3 SQL Link:*
[A3 SQL Link](docs/a3.sql "A3 SQL Link")


