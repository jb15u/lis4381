> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Jarod Barclay

### Assignment # Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs. 1,2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation My PHP Installation.
* Screenshot of running java Hello;
* Screenshot of running Android Studio - My First App
* git commands w/ short descriptions
* Bitbucket repo links: a) this assignment and b) the completed tutorials above

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- initialize .git inside current working directory
2. git status- checks status of git repository
3. git add- adds a file to git repository
4. git commit- saves changes to repository
5. git push- push commits from local branch to remote repository
6. git pull- pulls from repository to local branch 
7. git log- shows the git commit history for a project

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/jb15u/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/jb15u/myteamquotes/ "My Team Quotes Tutorial")
