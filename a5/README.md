# LIS4381

## Jarod Barclay

### Assignment 5 Requirements:

*Sub-Heading:*

1. Create a project using server-side validation
2. Chapter Questions (Chs. 11, 12)

#### README.md file should include the following items:

* Screenshot of index.php
* Screenshot of add_petstore_process.php



#### Assignment Screenshots:

*Screenshots:*

![Main Page Screenshot](img/s1.png)

![Invalid Screenshot](img/s2.png)


